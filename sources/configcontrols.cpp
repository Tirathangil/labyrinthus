#include "configcontrols.h"

video::E_DRIVER_TYPE GameConfigs::getDriverType(std::string Parameter)
{
    using namespace irr;
    using namespace std;

    if (Parameter == "opengl")
        return video::EDT_OPENGL;
    if (Parameter == "directx8")
        return video::EDT_DIRECT3D8;
    if (Parameter == "directx9")
        return video::EDT_DIRECT3D9;
    if (Parameter == "burningvideo")
        return video::EDT_BURNINGSVIDEO;
    if (Parameter == "null")
        return video::EDT_NULL;
    if (Parameter == "software")
        return video::EDT_SOFTWARE;

    return video::EDT_SOFTWARE;
}

bool GameConfigs::MakeDefaultConfig() //done. Works fine.
{
    using namespace libconfig;

    MainConfig.clear();
    Setting &RootSetting = MainConfig.getRoot();
    Setting &GraphicsGroup = RootSetting.add("GraphicSettings",Setting::Type::TypeGroup);
    GraphicsGroup.add("RenderSystem",Setting::Type::TypeString) = "software";
    Setting &Resolution = GraphicsGroup.add("Resolution",Setting::Type::TypeGroup);
    Resolution.add("Width",Setting::Type::TypeInt) = 680;
    Resolution.add("Height",Setting::Type::TypeInt) = 460;
    GraphicsGroup.add("ColorDepth",Setting::Type::TypeInt) = 32;
    GraphicsGroup.add("FullScreen",Setting::Type::TypeBoolean) = false;
    GraphicsGroup.add("Stencil",Setting::Type::TypeBoolean) = false;
    GraphicsGroup.add("VertSync",Setting::Type::TypeBoolean) = false;

    // Write error processing
    try
    {
        MainConfig.writeFile("main.conf");
    }
    catch(FileIOException &FileExcept)
    {
        std::cout << "write fails" << std::endl;
        return false;
    }

    return true;
}

bool GameConfigs::CheckSetting(const std::string SettingName,libconfig::Setting::Type SettingType) //done. Works fine.
{
    using namespace libconfig;

    if(!MainConfig.exists(SettingName))
    {
        std::cout << "Setting " << SettingName << " not found" << std::endl;
        return false;
    }
    //Setting &Check = MainConfig.lookup(SettingName);
    if (MainConfig.lookup(SettingName).getType() != SettingType)
    {
        std::cout << "Setting " << SettingName << " have bad type." << std::endl;
        return false;
    }

    return true;
}

bool GameConfigs::CheckConfig()
{
    using namespace libconfig;
    using namespace std;

    if(!CheckSetting("GraphicSettings",Setting::TypeGroup))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.RenderSystem",Setting::TypeString))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.Resolution",Setting::TypeGroup))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.Resolution.Width",Setting::TypeInt))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.Resolution.Height",Setting::TypeInt))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.ColorDepth",Setting::TypeInt))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.FullScreen",Setting::TypeBoolean))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.Stencil",Setting::TypeBoolean))
    {
        MakeDefaultConfig();
        return false;
    }
    if(!CheckSetting("GraphicSettings.VertSync",Setting::TypeBoolean))
    {
        MakeDefaultConfig();
        return false;
    }

    return true;
}
bool GameConfigs::SetVideoSettings()
{
    using namespace libconfig;

    Setting &RootSetting = MainConfig.getRoot();
    GraphSettings.RenderSystem = getDriverType(RootSetting.lookup("GraphicSettings.RenderSystem"));
    GraphSettings.Resolution.Width = RootSetting.lookup("GraphicSettings.Resolution.Width");
    GraphSettings.Resolution.Height = RootSetting.lookup("GraphicSettings.Resolution.Height");
    GraphSettings.ColorDepth = RootSetting.lookup("GraphicSettings.ColorDepth");
    GraphSettings.FullScreen = RootSetting.lookup("GraphicSettings.FullScreen");
    GraphSettings.Stencil = RootSetting.lookup("GraphicSettings.Stencil");
    GraphSettings.VertSync = RootSetting.lookup("GraphicSettings.VertSync");

    return true;
}
bool GameConfigs::ReadConfig (std::string Path)
{
    using namespace libconfig;

    try
    {
        MainConfig.readFile(ConfigPath.c_str());
    }
    catch(FileIOException &FileExcept)
    {
        if (!MakeDefaultConfig()) //if function was return "false" result, write process totally fails.
            return false;
    }
    catch (ParseException &ParseEx)
    {
        if (!MakeDefaultConfig()) //if function was return "false" result, write process totally fails.
            return false;
    }

    if(!CheckConfig())
    {
        std::cout << "Created default config file." << std::endl;
        return false;
    }

    if(!SetVideoSettings())
    {
        std::cout << "Unexpected error: videosettings not configured" << std::endl;
        return false;
    }

    return true;
}

bool GameConfigs::WriteConfig (std::string Path)
{

    return false; //tempo solution
}

void GameConfigs::SetConfPath (std::string Path)
{
    ConfigPath = Path;
}

GameConfigs::GameConfigs()
{

}

GameConfigs::~GameConfigs()
{

}

