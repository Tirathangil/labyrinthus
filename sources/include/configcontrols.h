#ifndef CONFIGCONTROLS_H_INCLUDED
#define CONFIGCONTROLS_H_INCLUDED

#include "irrlicht.h"
#include "libconfig.h++"
#include <string>
#include <iostream>

using namespace libconfig;
using namespace irr;

struct Graphics
{
    video::E_DRIVER_TYPE RenderSystem = video::EDT_OPENGL;
    core::dimension2d<u32> Resolution = core::dimension2d<u32>(640,480);
    u32 ColorDepth = 32;
    bool FullScreen = false;
    bool Stencil = false;
    bool VertSync = false;
};


class GameConfigs
{
    Config MainConfig;

    std::string ConfigPath = "main.conf";

    video::E_DRIVER_TYPE getDriverType(std::string);
    bool MakeDefaultConfig();
    bool CheckSetting(const std::string,libconfig::Setting::Type);
    bool CheckConfig();
    bool SetVideoSettings();

public:
    Graphics GraphSettings;

    bool ReadConfig (std::string Path = "main.conf");
    bool WriteConfig (std::string Path);
    void SetConfPath (std::string Path);
    GameConfigs();
    ~GameConfigs();
};



#endif // CONFIGCONTROLS_H_INCLUDED
