#include "irrlicht.h"
#include "configcontrols.h"
#include <iostream>

int main()
{
    using namespace irr;

    GameConfigs MainConf;

    MainConf.ReadConfig();

    IrrlichtDevice *RenderDevice = createDevice(MainConf.GraphSettings.RenderSystem,
                                                MainConf.GraphSettings.Resolution,
                                                MainConf.GraphSettings.ColorDepth,
                                                MainConf.GraphSettings.FullScreen,
                                                MainConf.GraphSettings.Stencil,
                                                MainConf.GraphSettings.VertSync);

    RenderDevice->setWindowCaption(L"Irrlicht урок #1");
    video::IVideoDriver *VideoDriver = RenderDevice ->getVideoDriver();
    scene::ISceneManager *MainScene = RenderDevice->getSceneManager();
    gui::IGUIEnvironment *TestGUI = RenderDevice->getGUIEnvironment();

    gui::IGUIStaticText *HWText = TestGUI->addStaticText(L"Russian not supported! Для теста",core::rect<s32>(10,10,100,50));
    HWText->setBackgroundColor(video::SColor(120,255,255,255));

    RenderDevice->getFileSystem()->addFileArchive("objects/firstlook.pk3");

    scene::IAnimatedMesh *MapMesh = MainScene->getMesh("maps/firstlook.bsp");
    scene::ISceneNode *MapNode = 0;

    if(MapMesh)
    {
        MapNode = MainScene->addOctreeSceneNode(MapMesh->getMesh(0),0,-1,1024);
    }

    if(MapNode)
    {
        MapNode->setPosition(core::vector3df(0,0,-10));
    }

    scene::ICameraSceneNode *FPSCamera = MainScene->addCameraSceneNodeFPS();
    RenderDevice->getCursorControl()->setVisible(false);

    int LastFPS=-1;

    while(RenderDevice->run())
    {
        if(RenderDevice->isWindowActive())
        {
            VideoDriver->beginScene(true,true,video::SColor(0, 51, 153, 255));
            MainScene->drawAll();
            VideoDriver->endScene();
            int FPS = VideoDriver->getFPS();

            if(LastFPS != FPS)
            {
                core::stringw TestStr = L"Irrlicht Engine. Tutorial 2. ";
                TestStr += VideoDriver->getName();
                TestStr += ".  FPS: ";
                TestStr += FPS;

                RenderDevice->setWindowCaption(TestStr.c_str());
                LastFPS = FPS;
            }
            else
            {
                RenderDevice->yield();
            }
        }
    }

    RenderDevice->drop();

    return 0;
}
